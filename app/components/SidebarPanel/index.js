/**
*
* SidebarPanel
*
*/

import React from 'react';
import Divider from 'material-ui/Divider';
import styled from 'styled-components';
import { ShieldIcon, CallIcon, LockIcon } from '../Icons';

const Header = styled.div`
  margin-top: 10px;
`;

const HeaderTitle = styled.div`
    line-height: 20px;
    text-transform: uppercase;
    color: rgb(68, 68, 68);
    font-size: 11px;
    font-weight: 600;
    letter-spacing: 0.125em;
    margin: 0px;
    padding: 0px;
`;

const PanelBody = styled.p`
    margin-top: -2px;
    margin-left: 30px;
    font-size: 11px;
    line-height: 17px;
    color: rgb(157, 160, 160);
    font-weight: 400;
`;


class SidebarPanel extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div style={{ float: 'left', marginTop: '5px' }}>
        <Divider />
        <Header>
          <ShieldIcon /> <HeaderTitle> PRIVACY </HeaderTitle>
        </Header>
        <PanelBody>We do not use or share your personal information beyond the purpose of assessing your loan application.</PanelBody>
        <div style={{ marginTop: '8px' }}>
          <Divider />
          <Header>
            <LockIcon /> <HeaderTitle> SAFE & SECURE </HeaderTitle>
          </Header>
          <PanelBody>128 bit SSL encryption. We use bank level security to keep all your data secure.</PanelBody>
        </div>
        <div style={{ marginTop: '8px' }}>
          <Divider />
          <Header>
            <CallIcon /> <HeaderTitle> NEED HELP </HeaderTitle>
          </Header>
          <PanelBody>Call us on 1800 4198 121 or Whatsapp <br /> on +91 8971 928 484</PanelBody>
        </div>
      </div>
    );
  }
}

SidebarPanel.propTypes = {

};

export default SidebarPanel;
