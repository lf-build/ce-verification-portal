/*
 * Faq Messages
 *
 * This contains all the text for the Faq component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Faq.header',
    defaultMessage: 'This is the Faq component !',
  },
});
