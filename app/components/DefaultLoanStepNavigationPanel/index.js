/**
*
* DefaultLoanStepNavigationPanel
*
*/

import React from 'react';
// import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';

class DefaultLoanStepNavigationPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.setState({ // eslint-disable-line react/no-did-mount-set-state
      nextButtonWidth: document.getElementById('btnNext').clientWidth,
    });
  }
  render() {
    const { isLoadingOrSubmiting, handleGoBack, handleGoNext, noBack, nextButtonName, valid, noBankStatement } = this.props;

    const nbWidth = nextButtonName && nextButtonName === 'Go to review' ? 128 : 88;

    return (
      <div>
        <div className={noBack ? 'hidden' : 'col-xs-6'}>
          <RaisedButton tabIndex={-1000} style={{ float: 'right', width: 'auto' }} disabled={isLoadingOrSubmiting} onTouchTap={handleGoBack} primary label="Back" labelColor={'#ffffff'} />
        </div>
        <div className={noBack ? 'col-xs-12' : 'col-xs-6'}>
          <RaisedButton
            id="btnNext"
            style={{ width: 'auto', float: 'left', ...(noBack ? { marginLeft: `calc(50% - (${this.state ? this.state.nextButtonWidth : nbWidth}px / 2))` } : {}) }}
            disabled={isLoadingOrSubmiting || !valid || noBankStatement}
            onTouchTap={handleGoNext}
            primary
            label={nextButtonName || 'Next'}
            labelColor={'#ffffff'}
          />
        </div>
      </div>);
  }
}

DefaultLoanStepNavigationPanel.propTypes = {
  isLoadingOrSubmiting: React.PropTypes.bool.isRequired,
  valid: React.PropTypes.bool.isRequired,
  handleGoBack: React.PropTypes.func.isRequired,
  handleGoNext: React.PropTypes.func.isRequired,
  nextButtonName: React.PropTypes.string,
  noBack: React.PropTypes.bool,
  noBankStatement: React.PropTypes.bool,
};

export default DefaultLoanStepNavigationPanel;
