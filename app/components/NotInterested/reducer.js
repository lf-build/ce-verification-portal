/*
 *
 * notInterested reducer
 *
 */

import { fromJS } from 'immutable';
import {
  NOT_INTERESTED_REQUEST_STARTED,
  NOT_INTERESTED_REQUEST_ENDED,
} from './constants';

const initialState = fromJS({});

function notInterestedReducer(state = initialState, action) {
  switch (action.type) {
    case NOT_INTERESTED_REQUEST_STARTED:
      return state
             .set('loading', true);
    case NOT_INTERESTED_REQUEST_ENDED:
      return state
             .set('loading', false);
    default:
      return state;
  }
}

export default notInterestedReducer;
