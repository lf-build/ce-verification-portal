/*
 *
 * NotIntereseted actions
 *
 */

import {
  NOT_INTERESTED_REQUEST,
  NOT_INTERESTED_REQUEST_STARTED,
  NOT_INTERESTED_REQUEST_FULFILLED,
  NOT_INTERESTED_REQUEST_REJECTED,
  NOT_INTERESTED_REQUEST_FAILED,
  NOT_INTERESTED_REQUEST_ENDED,
  NOT_INTERESTED_REQUEST_DENIED,
} from './constants';

export function notInterested(reason) {
  return {
    type: NOT_INTERESTED_REQUEST,
    meta: {
      reason,
    },
  };
}

export function notInterestedStarted() {
  return {
    type: NOT_INTERESTED_REQUEST_STARTED,
  };
}

export function notInterestedFulfilled(meta) {
  return {
    type: NOT_INTERESTED_REQUEST_FULFILLED,
    meta,
  };
}

export function notInterestedRejected() {
  return {
    type: NOT_INTERESTED_REQUEST_REJECTED,
  };
}

export function notInterestedFailed(meta) {
  return {
    type: NOT_INTERESTED_REQUEST_FAILED,
    meta,
  };
}

export function notInterestedDenied(meta) {
  return {
    type: NOT_INTERESTED_REQUEST_DENIED,
    meta,
  };
}

export function notInterestedEnded() {
  return {
    type: NOT_INTERESTED_REQUEST_ENDED,
  };
}
