/*
 *
 * NotIntereseted constants
 *
 */

export const DEFAULT_ACTION = 'app/NotInterested/DEFAULT_ACTION';

export const NOT_INTERESTED_REQUEST = 'app/NotInterested/NOT_INTERESTED_REQUEST';
export const NOT_INTERESTED_REQUEST_STARTED = 'app/NotInterested/NOT_INTERESTED_REQUEST_STARTED';
export const NOT_INTERESTED_REQUEST_FULFILLED = 'app/NotInterested/NOT_INTERESTED_REQUEST_FULFILLED';
export const NOT_INTERESTED_REQUEST_REJECTED = 'app/NotInterested/NOT_INTERESTED_REQUEST_REJECTED';
export const NOT_INTERESTED_REQUEST_FAILED = 'app/NotInterested/NOT_INTERESTED_REQUEST_FAILED';
export const NOT_INTERESTED_REQUEST_ENDED = 'app/NotInterested/NOT_INTERESTED_REQUEST_ENDED';
export const NOT_INTERESTED_REQUEST_DENIED = 'app/NotInterested/NOT_INTERESTED_REQUEST_DENIED';
