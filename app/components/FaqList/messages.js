/*
 * FaqList Messages
 *
 * This contains all the text for the FaqList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.FaqList.header',
    defaultMessage: 'FAQ',
  },
});
