import React from 'react';
import TextField from 'redux-form-material-ui/lib/TextField';
import MaskedInput from 'react-text-mask';
import { placeholderChar as defaultPlaceholderChar } from './masks';

class MaskedTextField extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { mask, pipe, defaultValue, placeholderChar, ...other } = this.props;
    // const { mask, pipe, defaultValue, placeholderChar, dontGuide, ...other } = this.props;
    const { input: { value }, readOnly, meta, type } = other;
    return (<TextField {...other} >
      <MaskedInput
        type={type}
        placeholderChar={placeholderChar || defaultPlaceholderChar}
        keepCharPositions
        readOnly={readOnly}
        value={defaultValue && meta.pristine ? defaultValue : value}
        guide={false}
        pipe={pipe}
        mask={mask}
      /></TextField>);
  }
}

MaskedTextField.propTypes = {
  // dontGuide: React.PropTypes.bool,
  mask: React.PropTypes.any,
  pipe: React.PropTypes.any,
  defaultValue: React.PropTypes.any,
  meta: React.PropTypes.any,
  placeholderChar: React.PropTypes.string,
};

export default MaskedTextField;
