/**
*
* ReviewApplicationNavigationPanel
*
*/

import React from 'react';
// import styled from 'styled-components';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';

export class ReviewApplicationNavigationPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { agree: false };
  }
  handleIAgree = (e, newAgree) => {
    this.setState({ agree: newAgree });
  }
  render() {
    const { isLoadingOrSubmiting, submitApplication } = this.props;
    const { agree } = this.state;
    const handleIAgree = this.handleIAgree;
    return (
      <div>
        <div className="row">
          <div className="col-xs-12">
            <Checkbox
              disabled={isLoadingOrSubmiting}
              onCheck={handleIAgree} checked={agree || isLoadingOrSubmiting}
              name="agree"
              label="I understand and agree to following terms:"
              labelStyle={{ fontSize: '15px', letterSpacing: '0.025em' }}
            />
            <ul style={{ marginTop: '10px', marginLeft: '55px', fontSize: '11px', lineHeight: '18px' }}>
              <li>I allow Qbera to verify my device location from my mobile operator.</li>
              {/* <li>I authorize Qbera and / or its affiliates to contact me. This will override registry on DND / NDNC.</li>*/}
              <li>I understand that Qbera and / or its affiliates will be pulling my credit report from the credit bureaus to process my loan.</li>
            </ul>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-5 col-xs-offset-4">
            <RaisedButton style={{ width: '70%', marginTop: '25px' }} disabled={isLoadingOrSubmiting || !agree} onTouchTap={submitApplication} primary label="Submit" />
          </div>
        </div>
      </div>);
  }
}

ReviewApplicationNavigationPanel.propTypes = {
  isLoadingOrSubmiting: React.PropTypes.bool.isRequired,
  // handleGoBack: React.PropTypes.func.isRequired,
  // handleGoNext: React.PropTypes.func.isRequired,
  submitApplication: React.PropTypes.func.isRequired,


};

export default ReviewApplicationNavigationPanel;
