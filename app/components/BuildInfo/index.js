/**
*
* BuildInfo
*
*/

import React from 'react';
import styled from 'styled-components';
import { version } from '../../../package.json';

const VersionWrapper = styled.span`
  position: fixed;
  align: top;
  top: 5px;
  right: 20px;
  fontSize: 9px;
  color: black;
  line-height: 1;
  display: ${process.env.SHOW_VERSION_INFO ? 'block' : 'none'}
`;

// Find a suitable way to show build numbers only during dev/qa/int.
// hidden={process.env.NODE_ENV !== 'development'}
const BuildInfo = () => <VersionWrapper> {version} </VersionWrapper>;

BuildInfo.propTypes = {

};

export default BuildInfo;
