import { put } from 'redux-saga/effects';
import {
  LOOKUP_REQUEST_REQUEST,
  LOOKUP_REQUEST_STARTED,
  LOOKUP_REQUEST_FULFILLED,
  LOOKUP_REQUEST_FAILED,
  LOOKUP_REQUEST_ENDED,
 } from './constants';

export const requestLookup = ({ entity, tag, searchText }) => ({
  type: LOOKUP_REQUEST_REQUEST,
  meta: {
    entity,
    tag,
    searchText,
  },
});

export const lookupStartedFor = (tag) => (put({
  type: LOOKUP_REQUEST_STARTED,
  meta: {
    tag,
  },
}));

export const lookupFulfilledFor = (tag, response = {}) => (put({
  type: `${LOOKUP_REQUEST_FULFILLED}_${tag}`,
  meta: { ...response },
}));

export const lookupFailedFor = (tag, meta = {}, error = {}) => (put({
  type: `${LOOKUP_REQUEST_FAILED}_${tag}`,
  meta: { ...meta, error },
}));

export const lookupEndedFor = (tag, meta = {}) => (put({
  type: `${LOOKUP_REQUEST_ENDED}_${tag}`,
  meta,
}));
