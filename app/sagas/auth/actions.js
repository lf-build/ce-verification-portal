/*
 *
 * SignInPage actions
 *
 */

import {
  DEFAULT_ACTION,
  USER_SIGNED_IN,
  USER_SIGNED_OUT,
  USER_INFO_AVAILABLE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function userSignedIn(token) {
  return {
    type: USER_SIGNED_IN,
    meta: {
      token,
    },
  };
}

export function userInfoAvailable(user) {
  return {
    type: USER_INFO_AVAILABLE,
    meta: {
      user,
    },
  };
}

export function userSignedOut() {
  return {
    type: USER_SIGNED_OUT,
  };
}
