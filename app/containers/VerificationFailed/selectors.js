import { createSelector } from 'reselect';

/**
 * Direct selector to the verificationFailed state domain
 */
const selectVerificationFailedDomain = () => (state) => state.get('verificationFailed');

/**
 * Other specific selectors
 */


/**
 * Default selector used by VerificationFailed
 */

const makeSelectVerificationFailed = () => createSelector(
  selectVerificationFailedDomain(),
  (substate) => substate.toJS()
);

export default makeSelectVerificationFailed;
export {
  selectVerificationFailedDomain,
};
