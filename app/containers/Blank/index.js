/*
 *
 * Blank
 *
 */

import React from 'react';
import Header from '../../components/Header';

export class Blank extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Header hideIdentitySessionPanel />
        <div className="container-fluid main no-padd">
          <div className="column middle">
            <div className={'col-lg-8 col-lg-offset-2'}>
              {React.Children.toArray(this.props.children)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Blank.propTypes = {
  children: React.PropTypes.node,
};

export default Blank;
