/*
 *
 * VerifyEmailPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  VERIFY_EMAIL_SET_ERROR_MSG,
} from './constants';

const initialState = fromJS({
  errorMsg: undefined,
});

function verifyEmailPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case VERIFY_EMAIL_SET_ERROR_MSG:
      return state.set('errorMsg', action.payload.errorMsg);
    default:
      return state;
  }
}

export default verifyEmailPageReducer;
