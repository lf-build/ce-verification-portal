/*
 *
 * VerifyEmailPage constants
 *
 */

export const DEFAULT_ACTION = 'app/VerifyEmailPage/DEFAULT_ACTION';
export const VERIFY_EMAIL_SET_ERROR_MSG = 'app/VerifyEmailPage/VERIFY_EMAIL_SET_ERROR_MSG';
export const VERIFY_EMAIL_REQUEST = 'app/VerifyEmailPage/VERIFY_EMAIL_REQUEST';
export const VERIFY_EMAIL_REQUEST_STARTED = 'app/VerifyEmailPage/VERIFY_EMAIL_REQUEST_STARTED';
export const VERIFY_EMAIL_REQUEST_FULFILLED = 'app/VerifyEmailPage/VERIFY_EMAIL_REQUEST_FULFILLED';
export const VERIFY_EMAIL_REQUEST_FAILED = 'app/VerifyEmailPage/VERIFY_EMAIL_REQUEST_FAILED';
export const VERIFY_EMAIL_REQUEST_ENDED = 'app/VerifyEmailPage/VERIFY_EMAIL_REQUEST_ENDED';

