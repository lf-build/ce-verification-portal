/*
 *
 * VerifyEmailPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';
import { withRouter } from 'react-router';

import makeSelectVerifyEmailPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import { generateVerifyEmailRequest as generateVerifyEmailRequestActionCreator } from './actions';

const VerifyEmailBlock = styled.div`
  color: #4e4e4e;
  font-size: 14px;
  padding: 10px;
  text-align: center;
`;

const errorMsgStyle = {
  position: 'relative',
  bottom: '15px',
  fontSize: '12px',
  lineHeight: '15px',
  color: 'rgb(255, 117, 106)',
  transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
  marginTop: '6px',
};

export class VerifyEmailPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (!this.props.params) {
      return <span />;
    }
    const { params: { token } } = this.props;
    const { VerifyEmailPage: { errorMsg }, verifyEmailCall } = this.props;

    const buildPayload = () => ({
      token,
    });

    return (
      <LoanStep
        payloadGenerator={buildPayload}
        saveErrorClass={'verify-email'}
        name={'verify-email'}
        title={messages.header}
        reduceWidth
        navigationPanel={(props) =>
          <div style={{ textAlign: 'center' }}>
            <div className={errorMsg ? '' : 'hidden'} style={{ ...errorMsgStyle }}>
              {errorMsg}
            </div>
            <RaisedButton disabled={props.isLoadingOrSubmiting} onTouchTap={() => verifyEmailCall(token)} primary label="Verify" />
          </div>}
      >
        <VerifyEmailBlock>
          Please click below button to verify your email.
        </VerifyEmailBlock>
      </LoanStep>
    );
  }
}

VerifyEmailPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  params: React.PropTypes.object,
  VerifyEmailPage: React.PropTypes.object,
  verifyEmailCall: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  VerifyEmailPage: makeSelectVerifyEmailPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    verifyEmailCall: (token) => dispatch(generateVerifyEmailRequestActionCreator(token)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(VerifyEmailPage));
