/*
 *
 * VerifyEmailPage actions
 *
 */

import {
  DEFAULT_ACTION,
  VERIFY_EMAIL_SET_ERROR_MSG,
  VERIFY_EMAIL_REQUEST,
  VERIFY_EMAIL_REQUEST_STARTED,
  VERIFY_EMAIL_REQUEST_FULFILLED,
  VERIFY_EMAIL_REQUEST_FAILED,
  VERIFY_EMAIL_REQUEST_ENDED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function verifyEmailSetErrorMsg(errorMsg) {
  return {
    type: VERIFY_EMAIL_SET_ERROR_MSG,
    payload: {
      errorMsg,
    },
  };
}

export function generateVerifyEmailRequest(token) {
  return {
    type: VERIFY_EMAIL_REQUEST,
    payload: {
      token,
    },
  };
}

export function generateVerifyEmailRequestStarted() {
  return {
    type: VERIFY_EMAIL_REQUEST_STARTED,
  };
}

export function generateVerifyEmailRequestFulfilled(offer) {
  return {
    type: VERIFY_EMAIL_REQUEST_FULFILLED,
    meta: {
      offer,
    },
  };
}

export function generateVerifyEmailRequestFailed(error) {
  return {
    type: VERIFY_EMAIL_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function generateVerifyEmailRequestEnded() {
  return {
    type: VERIFY_EMAIL_REQUEST_ENDED,
  };
}

