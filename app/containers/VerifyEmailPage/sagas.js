import { takeEvery, put } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { VERIFY_EMAIL_SET_ERROR_MSG, VERIFY_EMAIL_REQUEST } from './constants';
import * as verifyEmailActions from './actions';
import { navigationHelper } from '../NavigationHelper/helper';

function* verifyEmailFailed(action) {
  const { meta: { error: { body: { body } } } } = action;

  yield put({
    type: VERIFY_EMAIL_SET_ERROR_MSG,
    meta: {
      form: 'verify-email',
    },
    payload: {
      errorMsg: body.message,
    },
  });
}

function* verifyEmail(action) {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'authorization',
    section: 'identity',
    command: 'verify-email',
  }, {
    tag: 'verify-email',
    payload: { token: action.payload.token },
  }))) {
    return;
  }
  yield put(verifyEmailActions.generateVerifyEmailRequestStarted());
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('verify-email');
  if (valid) {
    yield put(verifyEmailActions.generateVerifyEmailRequestFulfilled(valid.meta.body));
    yield navigationHelper.goTo({
      name: 'verifyEmailPage',
      params: {
      },
    }, 'emailVerified');
  }

  yield put(verifyEmailActions.generateVerifyEmailRequestEnded());
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_FAILED_verify-email', verifyEmailFailed);
  yield takeEvery(VERIFY_EMAIL_REQUEST, verifyEmail);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
