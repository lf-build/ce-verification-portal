import { takeEvery, put } from 'redux-saga/effects';
import { SAVE_LOANSTEP_FULFILLED } from '../../containers/LoanStep/constants';
import { userSignedIn } from '../../sagas/auth/actions';

function* loginSaga(action) {
  yield put(userSignedIn(action.meta.response.meta.body.token));
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery(`${SAVE_LOANSTEP_FULFILLED}_create-account`, loginSaga);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
