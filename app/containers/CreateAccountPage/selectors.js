import { createSelector } from 'reselect';

/**
 * Direct selector to the createAccountPage state domain
 */
const selectCreateAccountPageDomain = () => (state) => state.get('createAccountPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by CreateAccountPage
 */

const makeSelectCreateAccountPage = () => createSelector(
  selectCreateAccountPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectCreateAccountPage;
export {
  selectCreateAccountPageDomain,
};
