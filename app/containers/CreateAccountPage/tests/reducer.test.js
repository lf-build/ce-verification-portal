
import { fromJS } from 'immutable';
import createAccountPageReducer from '../reducer';

describe('createAccountPageReducer', () => {
  it('returns the initial state', () => {
    expect(createAccountPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
