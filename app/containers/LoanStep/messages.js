/*
 * LoanStep Messages
 *
 * This contains all the text for the LoanStep component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.LoanStep.header',
    defaultMessage: 'This is LoanStep container !',
  },
});
