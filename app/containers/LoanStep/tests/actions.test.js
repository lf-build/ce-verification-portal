
import {
  clearError,
} from '../actions';
import {
  CLEAR_SAVE_FAILED,
} from '../constants';

describe('LoanStep actions', () => {
  describe('Default Action', () => {
    it('has a type of CLEAR_SAVE_FAILED', () => {
      const expected = {
        type: CLEAR_SAVE_FAILED,
      };
      expect(clearError()).toEqual(expected);
    });
  });
});
