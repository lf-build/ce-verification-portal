import { createSelector } from 'reselect';

/**
 * Direct selector to the loanStep state domain
 */
const selectLoanStepDomain = () => (state) => state.get('loanStep');

/**
 * Other specific selectors
 */


/**
 * Default selector used by LoanStep
 */

const makeSelectLoanStep = () => createSelector(
  selectLoanStepDomain(),
  (substate) => substate.toJS()
);

export default makeSelectLoanStep;
export {
  selectLoanStepDomain,
};
