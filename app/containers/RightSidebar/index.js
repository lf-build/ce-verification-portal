/*
 *
 * RightSidebar
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectRightSidebar from './selectors';
// import messages from './messages';
import FaqList from '../../components/FaqList';
import SidebarPanel from '../../components/SidebarPanel';

export class RightSidebar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { page } = this.props;
    return (
      <div>
        <FaqList {...{ page }} />
        <SidebarPanel />
      </div>
    );
  }
}

RightSidebar.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
};

const mapStateToProps = createStructuredSelector({
  RightSidebar: makeSelectRightSidebar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RightSidebar);
