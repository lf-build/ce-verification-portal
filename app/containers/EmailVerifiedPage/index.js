/*
 *
 * EmailVerifiedPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import makeSelectEmailVerifiedPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';

const VerifyEmailBlock = styled.div`
  color: #4e4e4e;
  font-size: 14px;
  padding: 10px;
  text-align: center;
`;

export class EmailVerifiedPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoanStep
        saveErrorClass={'verify-email'}
        name={'verify-email'}
        title={messages.header}
        reduceWidth
        navigationPanel={() => <span />}
      >
        <VerifyEmailBlock>
          Thanks for verifying your e-mail ID.
        </VerifyEmailBlock>
      </LoanStep>
    );
  }
}

EmailVerifiedPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  EmailVerifiedPage: makeSelectEmailVerifiedPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EmailVerifiedPage));
