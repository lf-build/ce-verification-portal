/*
 * EmailVerifiedPage Messages
 *
 * This contains all the text for the EmailVerifiedPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.EmailVerifiedPage.header',
    defaultMessage: 'Email Verified',
  },
});
