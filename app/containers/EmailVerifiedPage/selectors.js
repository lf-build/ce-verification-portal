import { createSelector } from 'reselect';

/**
 * Direct selector to the emailVerifiedPage state domain
 */
const selectEmailVerifiedPageDomain = () => (state) => state.get('emailVerifiedPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by EmailVerifiedPage
 */

const makeSelectEmailVerifiedPage = () => createSelector(
  selectEmailVerifiedPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEmailVerifiedPage;
export {
  selectEmailVerifiedPageDomain,
};
