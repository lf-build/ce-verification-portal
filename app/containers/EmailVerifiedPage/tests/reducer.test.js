
import { fromJS } from 'immutable';
import emailVerifiedPageReducer from '../reducer';

describe('emailVerifiedPageReducer', () => {
  it('returns the initial state', () => {
    expect(emailVerifiedPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
