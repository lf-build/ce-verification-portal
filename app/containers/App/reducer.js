/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */
import { fromJS } from 'immutable';

import applicationProgressReducer from '../ApplicationProgress/reducer';
import navigationHelperReducer from '../NavigationHelper/reducer';
import rightSidebarReducer from '../RightSidebar/reducer';
import loanStepReducer from '../LoanStep/reducer';
import authReducer from '../../sagas/auth/reducer';

import { NOT_INTERESTED_REQUEST_FULFILLED } from '../../components/NotInterested/constants';
import { LOAN_STEP_LOADED } from '../LoanStep/constants';

const initialState = fromJS({});
function appReducer(state = initialState, action) {
  switch (action.type) {
    case NOT_INTERESTED_REQUEST_FULFILLED:
      return state
            .set('status', 'not-interested');
    case LOAN_STEP_LOADED:
      if (action.meta.name === 'review-application') {
        return state
          .set('reviewPage', 'personalDetailsPage');
      }
      return state;
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default {
  app: appReducer,
  applicationProgress: applicationProgressReducer,
  navigationHelper: navigationHelperReducer,
  rightSidebar: rightSidebarReducer,
  loanStep: loanStepReducer,
  auth: authReducer,
};
