/*
 *
 * NavigationHelper
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import makeSelectNavigationHelper from './selectors';
import * as actions from './actions';
import { navigationHelper as helper } from './helper';

const mapStateToProps = createStructuredSelector({
  NavigationHelper: makeSelectNavigationHelper(),
});

function mapDispatchToProps(dispatch) {
  return {
//    dispatch,
    navigationActions: bindActionCreators(actions, dispatch),
  };
}

export default function withNavigationHelper(WrappedComponent) {
  return connect(mapStateToProps, mapDispatchToProps)(withRouter(class extends React.Component { // eslint-disable-line react/prefer-stateless-function

    static propTypes = {
 //     dispatch: PropTypes.func.isRequired,
      navigationActions: React.PropTypes.shape({
        disableRoute: React.PropTypes.func,
        navigateNext: React.PropTypes.func,
      }).isRequired,
      router: React.PropTypes.object,
      routes: React.PropTypes.array,
    }

    componentDidMount() {
      this.props.router.setRouteLeaveHook(this.props.routes[1], this.routerWillLeave(this.props.routes[1]));
    }

    routerWillLeave(currentRoute) {
      return (nextLocation) => this.canNavigateTo({ from: currentRoute.path, to: nextLocation });
    }

    canNavigateTo({ to, from }) {
      // console.group('navigationHelper: ');
      // console.info(`going from "${to}" to "${from}"`);
      // console.warn('Allowing all navigations in dev mode.');
      // console.groupEnd();
      return true || to || from;
    }

    render() {
      const thisRoute = this.props.routes[1];
      const { router, navigationActions: { navigateNext } } = this.props;
      const navigationHelper = {
        goNext(form, payloadGenerator = (() => ({})), overrideRoute) {
          navigateNext({
            name: overrideRoute || thisRoute.name,
            params: router.params,
            form,
            payloadGenerator,
          });
        },
        goBack() {
          helper.goBack(thisRoute.name);
        },
      };

      return (<WrappedComponent {...this.props} navigationHelper={navigationHelper} />);
    }
  }
  ));
}
