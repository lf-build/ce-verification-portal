import { createSelector } from 'reselect';

/**
 * Direct selector to the navigationHelper state domain
 */
const selectNavigationHelperDomain = () => (state) => state.get('navigationHelper');

/**
 * Other specific selectors
 */


/**
 * Default selector used by NavigationHelper
 */

const makeSelectNavigationHelper = () => createSelector(
  selectNavigationHelperDomain(),
  (substate) => substate.toJS()
);

export default makeSelectNavigationHelper;
export {
  selectNavigationHelperDomain,
};
