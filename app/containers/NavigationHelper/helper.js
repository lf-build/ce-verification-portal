import { browserHistory } from 'react-router';

const next = {
  createAccountPage: 'workDetailsPage',
  resetAccountPage: 'signInPage',
  verifyEmailPage: 'emailVerified',
  // emailVerified: 'signInPage',
};

const previous = {};

const pathByNames = {
  createAccountPage: 'application/create-account',
  resetAccountPage: 'user/reset-account',
  verifyEmailPage: 'user/verify-email',
  internalServerError: 'user/request-failed',
  welcomePage: 'user/welcome',
  emailVerified: 'user/email-verified',
};

export const navigationHelper = {
  goNext(route) {
    this.goTo(route, next[route.name]);
  },
  // goTo({ name, params: { id } }, nextRouteName) {
  goTo({ name }, nextRouteName) { // , preserveHistory = true) {
    if (nextRouteName && name !== nextRouteName) {
      // if (preserveHistory && false) {
      //   browserHistory.push(`/${pathByNames[nextRouteName]}`);
      // } else {
      browserHistory.replace(`/${pathByNames[nextRouteName]}`);
      // }
    }
  },
  /*
  * The browserHistory.replace supresses creation of browser history
  * hence we need to go to specific page when user clicks back.
  */
  goBack(currentRoute) {
    this.goTo({ name: currentRoute }, previous[currentRoute]);
    // browserHistory.goBack();
  },
};
