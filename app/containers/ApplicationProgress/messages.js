/*
 * ApplicationProgress Messages
 *
 * This contains all the text for the ApplicationProgress component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ApplicationProgress.header',
    defaultMessage: 'This is ApplicationProgress container !',
  },
});
