
import { fromJS } from 'immutable';
import applicationProgressReducer from '../reducer';

describe('applicationProgressReducer', () => {
  it('returns the initial state', () => {
    expect(applicationProgressReducer(undefined, {})).toEqual(fromJS({
      mlevel1: 0,
      mlevel2: 0,
      level1: 0,
      level2: 0,
      level3: 0,
    }));
  });
});
