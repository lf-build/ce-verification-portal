/*
 *
 * ResetAccountPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ResetAccountPage/DEFAULT_ACTION';
export const RESET_ACCOUNT_SET_ERROR_MSG = 'app/ResetAccountPage/RESET_ACCOUNT_SET_ERROR_MSG';

