import { takeEvery, put } from 'redux-saga/effects';
import { RESET_ACCOUNT_SET_ERROR_MSG } from './constants';

function* resetAccountFailed(action) {
  const { meta: { error: { body: { body } } } } = action;

  yield put({
    type: RESET_ACCOUNT_SET_ERROR_MSG,
    meta: {
      form: 'verify-email',
    },
    payload: {
      errorMsg: body.message && 'Generated token is already used.',
    },
  });
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_FAILED_reset-account', resetAccountFailed);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
