/*
 *
 * ResetAccountPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import RaisedButton from 'material-ui/RaisedButton';
import { withRouter } from 'react-router';

import makeSelectResetAccountPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';

const shortDescription = () => <p> Please select a password to save and retrieve your application. </p>;

const errorMsgStyle = {
  position: 'relative',
  bottom: '15px',
  fontSize: '12px',
  lineHeight: '15px',
  color: 'rgb(255, 117, 106)',
  transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
  marginTop: '6px',
};

export class ResetAccountPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (!this.props.params) {
      return <span />;
    }
    const { params: { token, username } } = this.props;
    const { ResetAccountPage: { resetAccErrorMsg } } = this.props;

    const buildPayload = () => ({
      token,
      username,
    });
    return (
      <LoanStep
        payloadGenerator={buildPayload}
        saveErrorClass={'reset-account'}
        name={'reset-account'}
        title={messages.header}
        ShortDescription={shortDescription}
        reduceWidth
        navigationPanel={(props) =>
          <div style={{ textAlign: 'center' }}>
            <div className={resetAccErrorMsg ? '' : 'hidden'} style={{ ...errorMsgStyle }}>
              {resetAccErrorMsg}
            </div>
            <RaisedButton disabled={props.isLoadingOrSubmiting} onTouchTap={props.handleGoNext} primary label="Submit" />
          </div>}
      >
        <TextField
          id="password"
          name="password"
          fullWidth
          hintText="Password"
          floatingLabelText="Password"
          type="password"
          minLen={6}
          maxLen={15}
        />

        <TextField
          id="confirmPassword"
          name="confirmPassword"
          fullWidth
          hintText="Confirm Password"
          floatingLabelText="Confirm Password"
          type="password"
          minLen={6}
          maxLen={15}
          optional
          extraValidators={[() => document.getElementById('password').value === document.getElementById('confirmPassword').value ? undefined : 'Passwords do not match']}
        />
      </LoanStep>
    );
  }
}

ResetAccountPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  params: React.PropTypes.object,
  ResetAccountPage: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  ResetAccountPage: makeSelectResetAccountPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ResetAccountPage));
