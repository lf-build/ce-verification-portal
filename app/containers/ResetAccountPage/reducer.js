/*
 *
 * ResetAccountPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  RESET_ACCOUNT_SET_ERROR_MSG,
} from './constants';

const initialState = fromJS({
  resetAccErrorMsg: undefined,
});

function resetAccountPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case RESET_ACCOUNT_SET_ERROR_MSG:
      return state.set('resetAccErrorMsg', action.payload.errorMsg);
    default:
      return state;
  }
}

export default resetAccountPageReducer;
