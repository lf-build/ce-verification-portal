/*
 *
 * ResetAccountPage actions
 *
 */

import {
  DEFAULT_ACTION,
  RESET_ACCOUNT_SET_ERROR_MSG,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function resetAccountSetErrorMsg(errorMsg) {
  return {
    type: RESET_ACCOUNT_SET_ERROR_MSG,
    payload: {
      errorMsg,
    },
  };
}
