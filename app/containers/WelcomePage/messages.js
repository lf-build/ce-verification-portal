/*
 * WelcomePage Messages
 *
 * This contains all the text for the WelcomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.WelcomePage.header',
    defaultMessage: 'This is WelcomePage container !',
  },
});
