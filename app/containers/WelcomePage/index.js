/*
 *
 * WelcomePage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectWelcomePage from './selectors';
import { HourglassIcon } from '../../components/Icons';
// import wf from '../../sagas/workflow/lib';
// import messages from './messages';
// import { navigationHelper } from '../NavigationHelper/helper';

export class WelcomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    setTimeout(() => { document.location = process.env.BORROWER_PORTAL_URL; }, 30000);
  }

  render() {
    return (
      /* <div id="welcomeSpinner" className="spinner">
        <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
        <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
        <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
      </div>*/
      <div style={{ height: '100vh', textAlign: 'center', display: 'table-cell', width: '100vw', verticalAlign: 'middle' }}>
        {/* <img alt="Loading animation" src="https://crex-cdn.s3.amazonaws.com/loaderqbera.gif" style={{ maxWidth: '450px', width: '100%' }} />*/}
        <HourglassIcon style={{ transform: 'scale(1.5)' }} />
        <div className="spinner">
          <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
          <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
          <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
        </div>
        <h1> Calculating your social score ... </h1>
      </div>
    );
  }
}

WelcomePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  WelcomePage: makeSelectWelcomePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomePage);
