// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: 'reset-account/:token/:username',
      name: 'resetAccountPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ResetAccountPage/reducer'),
          import('containers/ResetAccountPage/sagas'),
          import('containers/ResetAccountPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('resetAccountPage', reducer.default);
          injectSagas('resetAccountPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'verify-email/:token',
      name: 'verifyEmailPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/VerifyEmailPage/reducer'),
          import('containers/VerifyEmailPage/sagas'),
          import('containers/VerifyEmailPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('verifyEmailPage', reducer.default);
          injectSagas('verifyEmailPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: 'request-failed',
      name: 'internalServerError',
      getComponent(nextState, cb) {
        import('components/InternalServerError')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
    {
      path: 'social',
      name: 'welcomePage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/WelcomePage/reducer'),
          import('containers/WelcomePage/sagas'),
          import('containers/WelcomePage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('welcomePage', reducer.default);
          injectSagas('welcomePage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'social-failed',
      name: 'verificationFailed',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/VerificationFailed/reducer'),
          import('containers/VerificationFailed/sagas'),
          import('containers/VerificationFailed'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('verificationFailed', reducer.default);
          injectSagas('verificationFailed', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'email-verified',
      name: 'emailVerifiedPage',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/EmailVerifiedPage/reducer'),
          import('containers/EmailVerifiedPage/sagas'),
          import('containers/EmailVerifiedPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('emailVerifiedPage', reducer.default);
          injectSagas('emailVerifiedPage', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
